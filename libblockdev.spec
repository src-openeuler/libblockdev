# dmraid is deprecated
%define configure_opts --without-dmraid

Name:    libblockdev
Version: 3.0.4
Release: 9
Summary: libblockdev is a C library supporting GObject introspection for manipulation of block devices
License: LGPLv2+
URL:     https://github.com/storaged-project/libblockdev
Source0: https://github.com/storaged-project/libblockdev/releases/download/%{version}-1/%{name}-%{version}.tar.gz

Patch1: 0001-Add-BDPluginSpec-constructor-and-use-it-in-plugin_sp.patch
Patch2: 0002-Fix-leaking-error.patch
Patch3: 0003-lvm-dbus-Fix-leaking-error-in-bd_lvm_init.patch
Patch4: 0004-nvme-Fix-potential-memory-leak.patch
Patch5: 0005-part-Fix-potential-double-free-when-getting-parttype.patch
Patch6: 0006-crypto-Fix-double-free-in-bd_crypto_luks_remove_key.patch
Patch7: 0007-part-Fix-copy-paste-bug-in-bd_part_spec_copy.patch

BuildRequires: make glib2-devel libyaml-devel libbytesize-devel parted-devel libuuid-devel ndctl-devel device-mapper-devel
BuildRequires: device-mapper-devel systemd-devel nss-devel volume_key-devel >= 0.3.9-7 libblkid-devel libmount-devel
BuildRequires: cryptsetup-devel kmod-devel libxslt glib2-doc gtk-doc python3-devel gobject-introspection-devel
BuildRequires: autoconf-archive e2fsprogs-devel libnvme-devel keyutils-libs-devel
Requires:      btrfs-progs device-mapper device-mapper-multipath lvm2 mdadm
Requires:      device-mapper-persistent-data lvm2-dbusd >= 2.02.156 ndctl gdisk util-linux

Provides:  %{name}-utils%{?_isa} %{name}-utils
Obsoletes: %{name}-utils < %{version}
Provides:  %{name}-btrfs%{?_isa} %{name}-btrfs
Obsoletes: %{name}-btrfs < %{version}
Provides:  %{name}-crypto%{?_isa} %{name}-crypto
Obsoletes: %{name}-crypto < %{version}
Provides:  %{name}-dm%{?_isa} %{name}-dm
Obsoletes: %{name}-dm < %{version}
Provides:  %{name}-fs%{?_isa} %{name}-fs
Obsoletes: %{name}-fs < %{version}
Provides:  %{name}-kbd%{?_isa} %{name}-kbd
Obsoletes: %{name}-kbd < %{version}
Provides:  %{name}-vdo%{?_isa} %{name}-vdo
Obsoletes: %{name}-vdo < %{version}
Provides:  %{name}-loop%{?_isa} %{name}-loop
Obsoletes: %{name}-loop < %{version}
Provides:  %{name}-lvm%{?_isa} %{name}-lvm
Obsoletes: %{name}-lvm < %{version}
Provides:  %{name}-lvm-dbus%{?_isa} %{name}-lvm-dbus
Obsoletes: %{name}-lvm-dbus < %{version}
Provides:  %{name}-mdraid%{?_isa} %{name}-mdraid
Obsoletes: %{name}-mdraid < %{version}
Provides:  %{name}-mpath%{?_isa} %{name}-mpath
Obsoletes: %{name}-mpath < %{version}
Provides:  %{name}-nvdimm%{?_isa} %{name}-nvdimm
Obsoletes: %{name}-nvdimm < %{version}
Provides:  %{name}-part%{?_isa} %{name}-part
Obsoletes: %{name}-part < %{version}
Provides:  %{name}-swap%{?_isa} %{name}-swap
Obsoletes: %{name}-swap < %{version}
Provides:  %{name}-plugins-all%{?_isa} %{name}-plugins-all
Obsoletes: %{name}-plugins-all < %{version}

%description
libblockdev is a C library supporting GObject introspection for manipulation of block devices.
It has a plugin-based architecture where each technology (like LVM, Btrfs, MD RAID, Swap,...) is implemented in a separate plugin,
possibly with multiple implementations (e.g. using LVM CLI or the new LVM DBus API).

%package devel
Summary: Libraries and header files for libblockdev
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: glib2-devel device-mapper-devel xfsprogs dosfstools systemd-devel

Provides:  %{name}-btrfs-devel%{?_isa} %{name}-btrfs-devel
Obsoletes: %{name}-btrfs-devel < %{version}
Provides:  %{name}-crypto-devel%{?_isa} %{name}-crypto-devel
Obsoletes: %{name}-crypto-devel < %{version}
Provides:  %{name}-dm-devel%{?_isa} %{name}-dm-devel
Obsoletes: %{name}-dm-devel < %{version}
Provides:  %{name}-fs-devel%{?_isa} %{name}-fs-devel
Obsoletes: %{name}-fs-devel < %{version}
Provides:  %{name}-kbd-devel%{?_isa}  %{name}-kbd-devel
Obsoletes: %{name}-kbd-devel < %{version}
Provides:  %{name}-loop-devel%{?_isa} %{name}-loop-devel
Obsoletes: %{name}-loop-devel < %{version}
Provides:  %{name}-lvm-devel%{?_isa} %{name}-lvm-devel
Obsoletes: %{name}-lvm-devel < %{version}
Provides:  %{name}-lvm-dbus-devel%{?_isa} %{name}-lvm-dbus-devel
Obsoletes: %{name}-lvm-dbus-devel < %{version}
Provides:  %{name}-mdraid-devel%{?_isa} %{name}-mdraid-devel
Obsoletes: %{name}-mdraid-devel < %{version}
Provides:  %{name}-mpath-devel%{?_isa} %{name}-mpath-devel
Obsoletes: %{name}-mpath-devel < %{version}
Provides:  %{name}-nvdimm-devel%{?_isa} %{name}-nvdimm-devel
Obsoletes: %{name}-nvdimm-devel < %{version}
Provides:  %{name}-part-devel%{?_isa} %{name}-part-devel
Obsoletes: %{name}-part-devel < %{version}
Provides:  %{name}-swap-devel%{?_isa} %{name}-swap-devel
Obsoletes: %{name}-swap-devel < %{version}
Provides:  %{name}-vdo-devel%{?_isa} %{name}-vdo-devel
Obsoletes: %{name}-vdo-devel < %{version}
Provides:  %{name}-utils-devel%{?_isa} %{name}-utils-devel
Obsoletes: %{name}-utils-devel < %{version}

%description devel
Libraries and header files for a set for utils(libblockdev,libblockdev-btrfs,libblockdev-crypto,
libblockdev-dm,libblockdev-fs,libblockdev-kbd,libblockdev-loop,libblockdev-lvm,
libblockdev-lvm-dbus,libblockdev-mdraid,libblockdev-mpath,libblockdev-nvdimm,libblockdev-part,
libblockdev-swap,libblockdev-utils,libblockdev-vdo

%package tools
Summary:    Various nice tools based on libblockdev
Requires:   %{name}
Requires:   %{name}-lvm
BuildRequires: libbytesize-devel
Recommends: %{name}-lvm-dbus

%description tools
Various nice storage-related tools based on libblockdev.

%package -n python3-blockdev
Summary: Python3 bindings for libblockdev
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: python3-gobject-base
%{?python_provide:%python_provide python3-blockdev}

%description -n python3-blockdev
Python3 bindings for libblockdev

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ivf
%configure %{?configure_opts}
%make_build

%install
%make_install
find %{buildroot} -type f -name "*.la" | xargs %{__rm}

%ldconfig_scriptlets

%files
%license LICENSE
%{_libdir}/libblockdev.so.*
%{_libdir}/girepository*/BlockDev*.typelib
%{_libdir}/libbd_*.so.*
%dir %{_sysconfdir}/libblockdev
%dir %{_sysconfdir}/libblockdev/3/conf.d
%config %{_sysconfdir}/libblockdev/3/conf.d/00-default.cfg
%config %{_sysconfdir}/libblockdev/3/conf.d/10-lvm-dbus.cfg

%files devel
%{_libdir}/libbd_*.so
%{_libdir}/libblockdev.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gtk-doc/html/libblockdev
%{_datadir}/gir*/BlockDev*.gir
%dir %{_includedir}/blockdev
%{_includedir}/blockdev/*

%files tools
%{_bindir}/lvm-cache-stats
%{_bindir}/vfat-resize

%files -n python3-blockdev
%{python3_sitearch}/gi/overrides/*


%changelog
* Mon Aug 5 2024 cenhuilin <cenhuilin@kylinos.cn> - 3.0.4-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:part: Fix copy-paste bug in bd_part_spec_copy

* Mon Jul 22 2024 kouwenqi <kouwenqi@kylinos.cn> - 3.0.4-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix double free in crypto.c and part.c

* Fri Jul 12 2024 cenhuilin <cenhuilin@kylinos.cn> - 3.0.4-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:nvme: Fix potential memory leak

* Tue Jun 25 2024 liuh <liuhuan01@kylinos.cn> - 3.0.4-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:lvm-dbus: Fix leaking error in bd_lvm_init

* Sat May 11 2024 yanshuai <yanshuai@kylinos.cn> - 3.0.4-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:lvm-dbus: Fix leaking error

* Wed Apr 3 2024 wangzhiqiang <swangzhiqiang95@huawei.com> - 3.0.4-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add BDPluginSpec constructor and use it in plugin_specs_from_names

* Fri Mar 29 2024 sunhai <sunhai10@huawei.com> - 3.0.4-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: revert fix load plugin warning

* Wed Mar 27 2024 sunhai <sunhai10@huawei.com> - 3.0.4-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix load plugin warning

* Tue Feb 6 2024 wangzhiqiang <wangzhiqiang95@huawei.com> - 3.0.4-1
- upgrade version to 3.0.4

* Tue Jan 2 2024 liyanan <liyana61@h-partners.com> - 2.28-3
- remove require vdo

* Wed Mar 22 2023 wangzhiqiang <wangzhiqiang95@huawei.com> - 2.28-2
- remove require dmraid and kmod-kvdo

* Tue Dec 20 2022 wangzhiqiang <wangzhiqiang95@huawei.com> - 2.28-1
- update to libblockdev-2.28

* Wed Jun 15 2022 Hongtao Zhang <zhanghongtao22@huawei.com> - 2.26-2
- Add BuildRequire make

* Wed Nov 17 2021 Wenchao Hao <haowenchao@huawei.com> - 2.26-1
- update to libblockdev-2.26

* Tue Sep 28 2021 Wenchao Hao <haowenchao@huawei.com> - 2.24-8
- NOP:nothing but to make it able to sync between differnt branches

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.24-7
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Tue Jul 27 2021 yanglongkang <yannglongkang@huawei.com> - 2.24-6
- fix build fail caused by deprecated-declarations

* Sat Oct 31 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 2.24-5
- backport upstream patches-epoch2 to fix serveral problems

* Fri Oct 30 2020 yanglongkang <yanglongkang@huawei.com> - 2.24-4
- remove python2 dependency

* Tue Jul 28 2020 Ruijun Ge <geruijun@huawei.com> - 2.24-3
- backport upstream patches

* Wed Jul 15 2020 Zhiqiang Liu <lzhq28@mail.ustc.edu.cn> - 2.24-2
- download tar file from source0 in spec

* Mon Jun 29 2020 lixiaokeng<lixiaokeng@huawei.com> - 2.24-1
- Upgrade to 2.24.1

* Mon Sep 16 2019 wubo<wubo40@huawei.com> - 2.20.4
- Package init
